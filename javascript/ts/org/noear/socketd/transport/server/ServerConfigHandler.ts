/* Generated from Java with JSweet 3.1.0 - http://www.jsweet.org */
namespace org.noear.socketd.transport.server {
    /**
     * 服务端配置处理
     * 
     * @author noear
     * @since 2.0
     * @class
     */
    export interface ServerConfigHandler {
        (config: org.noear.socketd.transport.server.ServerConfig);
    }
}

