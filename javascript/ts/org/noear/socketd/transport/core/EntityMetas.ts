export namespace EntityMetas {

    /**
     * 框架版本号
     */
    export const META_SOCKETD_VERSION: string = "SocketD";

    /**
     * 数据长度
     */
    export const META_DATA_LENGTH: string = "Data-Length";

    /**
     * 数据类型
     */
    export const META_DATA_TYPE: string = "Data-Type";

    /**
     * 数据分片索引
     */
    export const META_DATA_FRAGMENT_IDX: string = "Data-Fragment-Idx";

    /**
     * 数据描述之文件名
     */
    export const META_DATA_DISPOSITION_FILENAME: string = "Data-Disposition-Filename";
}