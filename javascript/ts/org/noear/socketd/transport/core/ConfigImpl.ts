/* Generated from Java with JSweet 3.1.0 - http://www.jsweet.org */
namespace org.noear.socketd.transport.core {
    /**
     * 基础配置实现
     * 
     * @author noear
     * @since 2.0
     * @param {boolean} clientMode
     * @class
     * @extends org.noear.socketd.transport.core.ConfigBase
     */
    export class ConfigImpl extends org.noear.socketd.transport.core.ConfigBase<ConfigImpl> {
        public constructor(clientMode: boolean) {
            super(clientMode);
        }
    }
    ConfigImpl["__class"] = "org.noear.socketd.transport.core.ConfigImpl";
    ConfigImpl["__interfaces"] = ["org.noear.socketd.transport.core.Config"];


}

